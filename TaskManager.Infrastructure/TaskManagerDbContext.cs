﻿using Microsoft.EntityFrameworkCore;
using System;
using TaskManager.Core.Entities;

namespace TaskManager.Infrastructure
{
    public class TaskManagerDbContext: DbContext
    {
        public TaskManagerDbContext(DbContextOptions<TaskManagerDbContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskHistory> TaskHistorys { get; set; }

    }
}
