﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TaskManager.Core.Entities
{
   public class User
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Email { get; set; }
        [Required]
        [Column(TypeName = "varchar(10)")]
        public string Password { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Fullname { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string Mobileno { get; set; }
    }
}
